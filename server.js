
const express = require('express')
const server = express();
const port =3000;
const body_parser = require('body-parser');
server.use(body_parser.json());



server.get('/',(req,res) =>{
    res.sendFile(__dirname+'/index.html');
});

server.get('/items',(req,res)=>{
    res.json(db.movies.find());
});

server.get('/items/:id',(req,res)=>{
    const itemId =req.params.id;
    const item = db.movies.find({id:itemId});
    if(items.length){
        res.json(items);
    }
    else{
        res.json({message:`item ${itemId} doesn't exist`})
    }
});

server.post('/items',(req,res) =>{
    const item = req.body;
    console.log('Adding new item:',item);
    db.movies.save(item);
    res.json(db.movies.find());
});

server.put('/items/:id',(req,res)=>{
     const itemId =req.params.id;
     const item = req.body;
     console.log("Editing item:",ItemId,"to be",item);
     db.movies.update({id:itemId},item);
     res.json(db.movies.find());
 });

server.delete('/items/:id',(req,res)=>{
    const itemId =req.params.id;
    console.log("Delete items with id:",itemId);
    db.movies.remove({id:itemId});
    res.json(db.movies.find());
});

const db = require('diskdb');
db.connect('./data',['movies']);
if(!db.movies.find().length){
    const movie={"id":"tt0068646", "name": "The Godfather", "genre":"crime"};
    db.movies.save(movie);
}
console.log(db.movies.find());



server.listen(port,() => {
    console.log(`Server listening at ${port}`);
});